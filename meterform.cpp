#include "meterform.h"
#include "ui_meterform.h"

MeterForm::MeterForm(QWidget *parent) :
    QWidget(parent),m_waiting_fare(0),m_waiting_money(0),m_waiting_time(300),isPickup(false),
    ui(new Ui::MeterForm)
{
    ui->setupUi(this);
    m_timer = new QTimer(this);
    m_wait_time = QTime(0,0);
    m_timer->setInterval(1000);
    is_stop = true;
    connect(m_timer,SIGNAL(timeout()),this, SLOT(timer_eslapsed()));

    m_display_total = 16000;
    ui->lcdTotal->display(m_display_total);

    m_waiting_fare = 2000;
    m_waiting_money = 0;
    m_waiting_time = 30;

    m_delta_d = 0;

    m_wait_time = QTime(0,0);
#ifdef RPI
    meter = new GpioMeter();
    connect(meter,SIGNAL(event_report(double,double)),this,SLOT(process_meter(double,double)));
    meter->start();
#endif
    ui->btnMoney->setText("Bắt khách");

}

MeterForm::~MeterForm()
{
    delete ui;
}



void MeterForm::calculateFare(double distance)
{
    double A = 16000;    //A:  giá mở cửa 8000
    double B = 510;     //B: Số mét mở cửa 800m
    double C = 14200;    //C: Đơn giá nội đo( sau mở cửa) 11000
    double D = 30000;   //D: Số km tính giá nội đô : 20.000m
    double E = 11000;   //E: Giá ngoại đô: 10000
    double F = 50000;   //F: Km giá ngoại đô 2: 50.000m
    double G = 11000;    //G: Giá ngoại đô 2: 9000

    /* fare fomular
    If(KM<=B)  Tiền = A
    Else if(KM>=B&&KM<D)  Tiền = A + (KM-B)*C/1000}
    Else if(KM>=D&&KM<F)  Tiền = A + (D-B)*C/1000 + (KM –D)*E/1000
    Else if(KM>=F) Tiền = A + (D-B)*C/1000 + (F –D)*E/1000 + (KM-F)*G/1000
    */
    double fare = 0;
    if (distance < B)
    {
        fare = A;
        m_current_gap = 141;
        ui->lcdFare->display(C);
    }
    else if (distance >=B && distance < D)
    {
        m_current_gap = 141;
        //if (distance >= B && distance < B + m_current_gap)
        //    fare = A + (distance - B + m_current_gap)*C/1000;
        //else
        fare = A + (distance - B + m_current_gap)*C/1000;
        ui->lcdFare->display(C);
    }
    else if (distance >= D && distance < F)
    {
        m_current_gap = 178;
        //if (distance == D && distance < D + m_current_gap)
        fare = A + (D - B) * C/1000 + (distance - D + m_current_gap) * E/1000;
        //else
        //    fare = A + (D - B) * C/1000 + (distance - D) * E/1000;
        ui->lcdFare->display(E);
    }
    else if (distance >= F)
    {
        m_current_gap = 178;
        ui->lcdFare->display(E);
        //if (distance == F && distance < F + m_current_gap)
        fare = A + (D - B)*C/1000 + (F - D)*E/1000 + (distance - F + m_current_gap)*G/1000;
        //else
        //   fare = A + (D - B)*C/1000 + (F - D)*E/1000 + (distance - F)*G/1000;
    }
    //qDebug() << m_current_distance << fare << m_display_total;

    ui->lcdDistance->display(m_current_distance);

    if (fare - m_display_total >= 2000)
    {
        m_display_total += 2000;
        ui->lcdTotal->display(m_display_total + m_waiting_money);
    }

}

void MeterForm::timer_eslapsed()
{
    if (is_stop)
    {
        ui->lcdWaitTime->display(m_wait_time.toString("hh:mm:ss"));
        m_wait_time = m_wait_time.addSecs(1);
        if (m_wait_time.second()%m_waiting_time == 0 )
        {

            m_waiting_money += m_waiting_fare;
            ui->lcdTotal->display(m_display_total + m_waiting_money);
        }
    }
}

void MeterForm::process_meter(double distance,double velocty)
{
    m_current_distance = distance;
    calculateFare(m_current_distance);

    m_delta_d = 1;

    if (velocty == 0)
    {
        is_stop = true;
    }
    else if (velocty > 0)
    {
        is_stop = false;
    }
    if (velocty != -1)
        ui->lcdVelocty->display(velocty);
}

void MeterForm::on_btnImHere_clicked()
{
    m_timer->start();
#ifdef RPI
    meter->resetCounter();
    meter->start();
#endif
    ui->frmMeter->setEnabled(true);
    CoreInstance.imHere();
}

void MeterForm::on_btnCancel_clicked()
{
#ifdef RPI
    meter->terminate();
#endif
    m_timer->stop();

    m_display_total = 16000;
    ui->lcdTotal->display(m_display_total);

    m_waiting_fare = 2000;
    m_waiting_money = 0;
    m_waiting_time = 30;

    m_delta_d = 0;

    m_wait_time = QTime(0,0);
    ui->frmMeter->setEnabled(false);

    emit cancel();
}

void MeterForm::on_btnMoney_clicked()
{
    if (!isPickup)
    {
        isPickup = true;
        ui->btnMoney->setText("Tính tiền");
        CoreInstance.pickUp();
    }


}
