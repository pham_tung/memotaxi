#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include<QProcess>
#include <QProgressDialog>
#include "global.h"
#include "utils/QsKineticScroller.h"
#include "transport/tcptransportadapter.h"
#include "models/core.h"
#include "models/common.h"
#include "jobwidget.h"
#ifdef RPI
#include "hal/gpiometer.h"
#include <QX11EmbedContainer>
typedef QList<WId> WindowList;
#endif
using namespace models;
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

#ifdef RPI
    static WindowList windows();

    static WId findWindow(const QString& title);
    static QString windowTitle(WId window);
    static QStringList windowTitles();

    static uint idleTime();
#endif

    void clearLayout(QLayout *layout, bool deleteWidget = true);

public slots:
    void process_task(QVariantMap task);
    void populateJobs(QList<models::Job> jobs);
    void populateJobs();
    void job_click_handler(JobWidget* obj,QString action);
    void job_cancel();
    void bid_timeout();

private slots:
    void on_btnQuit_clicked();

    void on_btnMap_clicked();

    void on_btnInfo_clicked();

private:

    void process_job_cancel(JobWidget* jobWd);
    void process_job_bid(JobWidget* jobWd);
private:
    bool flag;
    QList<Job> current_jobs;
#ifdef RPI
    QX11EmbedContainer* container;
    QProcess *pNavit;
#endif
    QsKineticScroller* scrollerHelper;
    QProgressDialog progress;
private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
