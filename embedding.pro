#-------------------------------------------------
#
# Project created by QtCreator 2015-06-05T12:08:09
#
#-------------------------------------------------

QT       += core gui dbus network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = embedding
TEMPLATE = app
CONFIG-=app_bundle
CONFIG   += extserialport

SOURCES += main.cpp \
    loginform.cpp \
    transport/tcptransportadapter.cpp \
    config/deviceconfig.cpp \
    virtualkeyboard.cpp \
    models/command.cpp \
    models/core.cpp \
    utils/QsKineticScroller.cpp \
    jobwidget.cpp \
    dialog.cpp \
    meterform.cpp \
    hal/gpiometer.cpp \
    hal/gpsd.cpp \
    driverinfodialog.cpp \
    hal/clock.cpp


HEADERS  += \
    loginform.h \
    transport/transportbase.h \
    transport/tcptransportadapter.h \
    global.h \
    config/deviceconfig.h \
    virtualkeyboard.h \
    models/response.h \
    models/command.h \
    models/common.h \
    models/core.h \
    utils/singleton.h \
    utils/call_once.h \
    utils/QsKineticScroller.h \
    models/demo_gps.h \
    checker.h \
    jobwidget.h \
    dialog.h \
    meterform.h \
    hal/gpiometer.h \
    hal/gpsd.h \
    rpi.h \
    driverinfodialog.h \
    hal/clock.h


FORMS    += \
    loginform.ui \
    jobwidget.ui \
    dialog.ui \
    meterform.ui \
    driverinfodialog.ui

LIBS += -lX11 -lqjson -L/usr/local/lib -lwiringPi -lgps

OTHER_FILES += settings.ini \
    a.log

#unix: LIBS += -L$$PWD/qjson-build/lib/ -lqjson

INCLUDEPATH += $$PWD/qjson/include
DEPENDPATH += $$PWD/qjson-build

DISTFILES += \
    gps.csv \
    delete.png \
    test.sh
