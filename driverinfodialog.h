#ifndef DRIVERINFODIALOG_H
#define DRIVERINFODIALOG_H

#include <QDialog>

namespace Ui {
class DriverInfoDialog;
}

class DriverInfoDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DriverInfoDialog(QWidget *parent = 0);
    ~DriverInfoDialog();

private:
    Ui::DriverInfoDialog *ui;
};

#endif // DRIVERINFODIALOG_H
