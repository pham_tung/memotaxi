#ifndef GLOBAL_H
#define GLOBAL_H
#include "models/core.h"
#include "utils/singleton.h"

#define CoreInstance Singleton<models::DeviceCore>::instance()

#endif // GLOBAL_H
