#include "loginform.h"
#include "ui_loginform.h"
#include "rpi.h"
#include "global.h"
#include "QDir"
#ifndef RPI
#include "qjson/serializer"
#include "qjson/parser"
#else
#include "qjson/serializer.h"
#include "qjson/parser.h"
#endif
LoginForm::LoginForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginForm)
{
    ui->setupUi(this);
    keyboard = new VirtualKeyboard(this);
    QFont font;
    font.setPointSize(20);
    font.setBold(false);
    font.setItalic(false);
    font.setWeight(50);
    keyboard->setFont(font);
    ui->kbdLayout->addWidget(keyboard);
    keyboard->toggleKeyboard();
    ui->txtPassword->installEventFilter(this);
    ui->txtUsername->installEventFilter(this);
}

LoginForm::~LoginForm()
{
    delete tcp_adapter;
    delete cfg;
    delete ui;
}

void LoginForm::on_btnLogin_clicked()
{
    QString warning;
    if (ui->txtUsername->text().length() == 0)
    {
        if (ui->txtPassword->text().length() == 0)
            warning ="Username & Password can not be null!";
        else
            warning = "Username can not be null!";
    }
    else if (ui->txtPassword->text().length() == 0)
    {
        warning = "Password can not be null!";
    }
    else
    {
        CoreInstance.loginNew(ui->txtUsername->text(),ui->txtPassword->text());
//        {
//            this->hide();
//            // switch main form
//            Form* form = new Form(/*0,tcp_adapter*/);
//            form->show();
//        }
    }

    //ui->lbStatus->text() = warning;

}

bool LoginForm::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
    {
        if (object->inherits("QLineEdit"))
        {
            keyboard->setInput(object);
        }
    }
    return false;

}

