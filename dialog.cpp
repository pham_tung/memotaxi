#include "dialog.h"
#include "ui_dialog.h"
#include <QLayoutItem>
#include<QTimer>
#include<QDebug>
#include "global.h"
#ifdef RPI
#include<QX11Info>
#include<X11/Xutil.h>
#include<QtDBus/QDBusMessage>
#include<QtDBus/QDBusConnection>
#endif

using namespace models;

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    scrollerHelper = new QsKineticScroller(this);
    scrollerHelper->enableKineticScrollFor(ui->scrollArea);
    ui->verticalLayout_2->setAlignment(Qt::AlignTop);
    connect(CoreInstance.getAdapter(),SIGNAL(got_task(QVariantMap)),
            this,SLOT(process_task(QVariantMap)));
    connect(ui->meterWd,SIGNAL(cancel()),this,SLOT(job_cancel()));
    //populateJobs();
    progress.setParent(this);
    progress.setWindowModality(Qt::WindowModal);
    //progress.setLabelText("......");
    progress.setCancelButton(0);
    progress.setRange(0,0);
    progress.setMinimumDuration(0);
    progress.hide();
#ifdef RPI
    container = new QX11EmbedContainer(ui->container);

    WId id = findWindow("Navit");
    if (id == 0)
    {


        pNavit = new QProcess(this);

        pNavit->start("navit");
        sleep(1);
        id = findWindow("Navit");
    }

    QDBusMessage m = QDBusMessage::createMethodCall("org.navit_project.navit",
                                                    "/org/navit_project/navit/default_navit",
                                                    "org.navit_project.navit.navit","clear_destination");

    bool queued = QDBusConnection::sessionBus().send(m);
    container->embedClient(id);
    container->resize(ui->container->size());
    ui->containerMain->setEnabled(true);

    CoreInstance.startGps();
#endif
}

Dialog::~Dialog()
{
#ifdef RPI
    if (pNavit!= NULL)
    {
        //pNavit->close();
        pNavit->terminate();
        delete pNavit;
    }

    if (container)
        delete container;
#endif
    if (scrollerHelper)
        delete scrollerHelper;
    if (ui)
        delete ui;
}

void Dialog::populateJobs(QList<Job> jobs)
{
    current_jobs.clear();
    current_jobs.append(jobs);
    populateJobs();
}

void Dialog::populateJobs()
{


    clearLayout(ui->verticalLayout_2);

    Q_FOREACH(Job job,current_jobs)
    {

        JobWidget* jobWd = new JobWidget (ui->scrollArea,&job);
        connect(jobWd,SIGNAL(btn_clicked(JobWidget*,QString)),this,SLOT(job_click_handler(JobWidget*,QString)));
        ui->verticalLayout_2->addWidget(jobWd);
    }

}

void Dialog::job_click_handler(JobWidget *obj, QString action)
{
    if (action == "Cancel")
    {
        process_job_cancel(obj);
    }
    else if (action == "Bid")
    {
        process_job_bid(obj);
    }

}

void Dialog::job_cancel()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void Dialog::process_job_cancel(JobWidget *jobWd)
{
#ifdef RPI
    QDBusMessage m = QDBusMessage::createMethodCall("org.navit_project.navit",
                                                    "/org/navit_project/navit/default_navit",
                                                    "org.navit_project.navit.navit","clear_destination");

    bool queued = QDBusConnection::sessionBus().send(m);
#endif
    Q_FOREACH(Job job,current_jobs)
    {
        if (job.OrderId() == jobWd->Job()->OrderId())
        {
            current_jobs.removeOne(job);
            break;
        }

    }
    populateJobs();
}

void Dialog::bid_timeout()
{
    qDebug() << "timeout";
    populateJobs();
}

void Dialog::process_job_bid(JobWidget *jobWd)
{
#ifdef RPI
    //105.83201128989458,
    //21.019716470438016
    QDBusMessage m = QDBusMessage::createMethodCall("org.navit_project.navit",
                                                    "/org/navit_project/navit/default_navit",
                                                    "org.navit_project.navit.navit","set_destination");
    m << QString("geo: ")+ QString::number(jobWd->Job()->From().Longitude()) + " " + QString::number(jobWd->Job()->From().Latitude());
    m << "comment";
    bool queued = QDBusConnection::sessionBus().send(m);
#endif
    //if (queued)
    //flag != flag;
    CoreInstance.bid(jobWd->Job());
    for (int i = 0; i < ui->verticalLayout_2->count(); ++i)
    {

        QWidget *widget = ui->verticalLayout_2->itemAt(i)->widget();
        if (widget == jobWd)
                widget->setEnabled(true);
        else
                widget->setEnabled(true);
    }
    // show progress dialog
    //    progress.show();
    QTimer timer;
    timer.setInterval(5000);
    timer.setSingleShot(true);
    connect(&timer,SIGNAL(timeout()),this,SLOT(bid_timeout()));
    timer.start();
}

void Dialog::clearLayout(QLayout* layout, bool deleteWidget)
{
    while(QLayoutItem* item = layout->takeAt(0))
    {
        if (deleteWidget)
            if (QWidget* widget = item->widget())
                delete widget;
        if (QLayout* childlayout = item->layout())
            clearLayout(childlayout,deleteWidget);
        delete item;
    }
}

void Dialog::process_task(QVariantMap task)
{
    QString command = task["command"].toString();
    qDebug() << "process task1: " << command;
    if (command == supported_command[UpdateJob])
    {
        QList<Job> jobs;
        QList<QVariant> list = ((QVariant)task["LstJob"]).toList();
        Q_FOREACH(QVariant j,list)
        {
            Job curJob;

            QVariantMap jMap =  j.toMap();
            curJob.setOrderId(jMap["OrderId"].toLongLong());
            curJob.setFrom(Location(jMap["From"]));
            curJob.setTo(Location(jMap["To"]));
            qDebug() << jMap["Distance"].toString().toDouble();
            curJob.setDistance(jMap["Distance"].toString().toDouble());
            curJob.setOrderState(jMap["State"].toInt());
            curJob.setBid(jMap["IsBid"].toBool());
            curJob.setSchedule(jMap["IsSchedule"].toBool());
            qDebug() << jMap["NoteToDriver"].toString();
            curJob.setNote(jMap["NoteToDriver"].toString());
            jobs.append(curJob);
        }
        populateJobs(jobs);
    }
    else if (command == supported_command[FinishOrder])
    {
        ui->stackedWidget->setCurrentIndex(1);
    }
    else if (command == supported_command[CancelOrder])
    {
        ui->stackedWidget->setCurrentIndex(0);
        populateJobs();
    }
    else if (command == supported_command[NotSelected])
    {
        ui->stackedWidget->setCurrentIndex(0);
        populateJobs();
    }

}

#ifdef RPI
WId Dialog::findWindow(const QString& title)
{
    Window result = 0;
    WindowList list = windows();
    foreach (const Window &wid, list)
    {
        if (windowTitle(wid) == title)
        {

            result = wid;
            break;
        }
    }
    return result;
}

QString Dialog::windowTitle(WId window)
{
    QString name;
    char* str = 0;
    if (XFetchName(QX11Info::display(), window, &str))
        name = QString::fromLatin1(str);
    if (str)
        XFree (str);
    //qDebug() << name;
    return name;
}
static WindowList qxt_getWindows(Atom prop)
{
    WindowList res;
    Atom type = 0;
    int format = 0;
    uchar* data = 0;
    ulong count, after;
    Display* display = QX11Info::display();
    Window window = QX11Info::appRootWindow();
    if (XGetWindowProperty(display, window, prop, 0, 1024 * sizeof(Window) / 4, False, AnyPropertyType,
                           &type, &format, &count, &after, &data) == Success)
    {
        Window* list = reinterpret_cast<Window*>(data);
        for (uint i = 0; i < count; ++i)
            res += list[i];
        if (data)
            XFree(data);
    }
    return res;
}
WindowList Dialog::windows()
{
    static Atom net_clients = 0;
    if (!net_clients)
        net_clients = XInternAtom(QX11Info::display(), "_NET_CLIENT_LIST_STACKING", True);

    return qxt_getWindows(net_clients);
}
#endif

void Dialog::on_btnQuit_clicked()
{
#ifdef RPI
    close();
#endif
}

void Dialog::on_btnMap_clicked()
{
    ui->containerMain->setCurrentIndex(0);
}

void Dialog::on_btnInfo_clicked()
{
    ui->containerMain->setCurrentIndex(1);
}
