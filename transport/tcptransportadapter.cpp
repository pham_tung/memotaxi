#include "tcptransportadapter.h"
#include "global.h"
#ifdef RPI
#include <QRegExp>
#include <qjson/parser.h>
#else
#include <QRegularExpression>
#include <qjson/parser>
#endif


TcpTransportAdapter::TcpTransportAdapter()
{
    m_ip = "";
    m_port = 0;
    m_ready = false;
}

TcpTransportAdapter::TcpTransportAdapter(QString ip,uint port)
{
    m_ip = ip;
    m_port = port;
    m_ready = false;
    socket = new QTcpSocket();
}

TcpTransportAdapter::~TcpTransportAdapter()
{
    qDebug() << "destroying socket..";
    if (!socket && socket->isOpen())
    {
        socket->close();
        delete socket;
        qDebug() << "socket destroyed!";
    }
    //    if (this->isRunning())
    //        this->terminate();
}

int TcpTransportAdapter::open()
{
    if (m_ip.isNull() || !m_port)
        return -1;

    if (socket != NULL && socket->isOpen())
    {
        socket->abort();
        delete socket;
    }
    if (socket == NULL)
        socket = new QTcpSocket();
    socket->setParent(0);
    //    if (thread != 0)
    //        socket->moveToThread(thread);
    connect( socket, SIGNAL(connected()),this, SLOT(connected()),Qt::DirectConnection );
    connect( socket, SIGNAL(disconnected()),this, SLOT(disconnected()),Qt::DirectConnection );
    connect( socket, SIGNAL(readyRead()),this, SLOT(dataReady()),Qt::DirectConnection );


    qDebug() << "Adapter connecting to: " << m_ip << ":" << m_port;
    socket->connectToHost(m_ip, m_port);
    if(socket->waitForConnected(5000)) {
        qDebug() << "Adapter started at: " << m_ip << ":" << m_port;
        m_ready = true;
        return 0;
    }
    qDebug() << "Error: " << socket->errorString();

    return -1;
}

void TcpTransportAdapter::dataReady()
{

    //qDebug() << "tcp read data";
    data = socket->readAll();
    //qDebug() << data;
#if (QT_VERSION > QT_VERSION_CHECK(4, 8, 0))
    QRegExp regex(REGEX_RESPONSE_4_8);
    //QString data1 = "_W_C_B_CarLoginByToken@@{\"Error\":1,\"DriverName\":null,\"DriverId\":0,\"DriverState\":0,\"Phone\":null,\"Avatar\":null,\"Rate\":0,\"Account\":null,\"PersonalID\":null,\"Email\":null,\"PIDProvince\":null,\"LicenseNumber\":null,\"VehiclePlate\":null,\"VehicleModel\":null,\"Seat\":0,\"Airport\":false,\"Password\":null,\"Token\":null}_W_C_E_ \
    //         ";
    if (regex.exactMatch(data))
    {
        //qDebug() << "match";
        if  (regex.captureCount() != 2)
        {
          //  qDebug() << regex.captureCount();
            return;
        }
        //If Regex does match - Works!
        QString command = regex.capturedTexts()[1];
        QString content = regex.capturedTexts()[2];
        //qDebug() << command ;
        //         qDebug()<< content;
        // create a JSonDriver instance
        QJson::Parser parser;
        bool ok;
        // json is a QString containing the data to convert
        QVariantMap task = parser.parse(content.toLatin1(), &ok).toMap();
        if (ok)
        {
            //qDebug() << "parse";
            task.insert("command",command);
            emit got_task(task);
        }

    }
#else

    //qDebug() << data;
    QRegularExpression regex(REGEX_RESPONSE);

    QRegularExpressionMatch match = regex.exactMatch(data);
    if(match.hasMatch())
        //if (regex.exactMatch(data))
    {
        //qDebug() << "match";
        //If Regex does match - Works!
        QString command = match.captured("command");
        QString content = match.captured("content");
        // create a JSonDriver instance
        QJson::Parser parser;
        bool ok;
        // json is a QString containing the data to convert
        QVariantMap task = parser.parse(content.toLatin1(), &ok).toMap();
        if (ok)
        {
            //qDebug() << "parse";
            task.insert(0,"command",command);
            emit got_task(task);
        }

    }
#endif


}

void TcpTransportAdapter::connected()
{
    qDebug() << "connected!";
    m_ready = true;
}


void TcpTransportAdapter::disconnected()
{
    qDebug() << "disconnected!";
    m_ready = false;
}

int TcpTransportAdapter::read(char* data, int maxlength)
{
    if (socket && socket->isOpen() )
    {
        if (! socket->isReadable())
            socket->waitForReadyRead();

        return socket->read(data,maxlength);
    }
    return 0;
}

int TcpTransportAdapter::write(const char *buffer)
{

    if (socket && socket->isOpen() && socket->isWritable())
    {
        quint64 ret = socket->write(buffer);

        qDebug() << "write" << ret << buffer;
        socket->waitForBytesWritten();
//        if (socket->waitForReadyRead())
//        {
//            qDebug() << "write read data";
//            data = socket->readAll();
//            qDebug() << data;
//        }

        return ret;
    }
    return 0;
}

void TcpTransportAdapter::close()
{
    if (!socket && socket->isOpen())
    {
        socket->close();
        delete socket;
    }
}

bool TcpTransportAdapter::ready()
{
    return m_ready;
}

void TcpTransportAdapter::moveToThread(QThread* thread)
{
    socket->moveToThread(thread);
}

//int TcpTransportAdapter::writeAsync(QByteArray buffer)
//{
//    return write(buffer.toStdString().c_str());
//}

//QByteArray TcpTransportAdapter::writeForResponse(const char *buffer)
//{
//    qDebug() << "start writing" << buffer;
//    //if (writeAsync(buffer) == buffer.length())
//    if (write(buffer))
//    {
//        qDebug() << "writing ok";

//        m_Response = "";
//        is_sync = true;
//        if (adapter_sync.tryLock())
//        {
//            qDebug() << "wait for response";

//            adapter_cond.wait(&adapter_sync);
//            adapter_sync.unlock();
//            qDebug() << "got response" << m_Response;
//            return m_Response;
//        }

//    }
//    return NULL;
//}

//void TcpTransportAdapter::processMessage(QByteArray message)
//{
//    qDebug() << "proc mess";
//    if (is_sync)
//    {
//        qDebug() << "syncccc";
//        adapter_sync.lock();
//        m_Response = message;
//        adapter_cond.wakeAll();
//        adapter_sync.unlock();
//        is_sync = false;
//    }
//    else

//        emit got_message(message);
//}
