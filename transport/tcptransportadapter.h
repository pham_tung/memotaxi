#ifndef TCPTRANSPORTADAPTER_H
#define TCPTRANSPORTADAPTER_H
#include <QtNetwork/QTcpSocket>
#include <QByteArray>
#include <QObject>
#include <QWaitCondition>
#include "transport/transportbase.h"

class TcpTransportAdapter :public QObject, public ITransportable
{
    Q_OBJECT

private:
    QTcpSocket* socket;
    QString m_ip;
    uint m_port;
    bool m_ready;

    QByteArray data;

public:
    explicit TcpTransportAdapter();
    TcpTransportAdapter(QString ip = "",uint port = 0);
    ~TcpTransportAdapter();

signals:
    void got_task(QVariantMap task);
//private:
//    void processMessage(QByteArray message);
public:
    int open();
    int read(char* data, int maxlength);
    int write(const char *buffer);
    void close();
    bool ready();

//    int writeAsync(QByteArray buffer);
//    QByteArray writeForResponse(const char* buffer);

public slots:
    void moveToThread(QThread* thread);
    void dataReady();
    void connected();
    void disconnected();

};

#endif // TCPTRANSPORTADAPTER_H
