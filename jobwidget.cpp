#include "jobwidget.h"
#include "ui_jobwidget.h"
#include <QDebug>
#include <QPainter>

JobWidget::JobWidget(QWidget *parent,models::Job* job) :
    QWidget(parent),
    ui(new Ui::JobWidget)
{
    ui->setupUi(this);
    m_job = new models::Job();
    m_job->setBid(job->Bid());
    m_job->setDistance(job->Distance());
    m_job->setFrom(job->From());
    m_job->setTo(job->To());
    m_job->setNote(job->NoteToDriver());
m_job->setOrderId(job->OrderId());
m_job->setOrderState(job->OrderState());
m_job->setSchedule(job->Schedule());
qDebug() << m_job->From().toString();
    ui->lbName->setText(job != 0?job->From().Name():"dummy");
    ui->lbAddress->setText(job != 0?job->To().Name():"dummy");
    qDebug() << QString("%L0 Km").arg(job->Distance(),0,'f',2);
    ui->lbDistance->setText(job != 0?QString("%L0 Km").arg(job->Distance(),0,'f',2):"0 Km" );
    ui->btnBid->setEnabled(job != 0?!job->Bid():true);
}

JobWidget::~JobWidget()
{
    delete ui;
}

void JobWidget::paintEvent(QPaintEvent *)
{
    QStyleOption opt;
    opt.init(this);
    QPainter p(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

void JobWidget::on_btnBid_clicked()
{
    emit btn_clicked(this,"Bid");
}

void JobWidget::on_btnCancel_clicked()
{
    emit btn_clicked(this,"Cancel");
}
