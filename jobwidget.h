#ifndef JOBWIDGET_H
#define JOBWIDGET_H

#include <QWidget>
#include "models/common.h"
namespace Ui {
class JobWidget;
}

class JobWidget : public QWidget
{
    Q_OBJECT

public:
    explicit JobWidget(QWidget *parent = 0, models::Job* data = 0);
    ~JobWidget();
    models::Job* Job()const{return m_job;}
signals:
    void btn_clicked(JobWidget* obj,QString action);

protected:
    void paintEvent(QPaintEvent *);
//    QSize sizeHint() const;
private slots:
    void on_btnBid_clicked();

    void on_btnCancel_clicked();

private:
    Ui::JobWidget *ui;
    models::Job* m_job;
};

#endif // JOBWIDGET_H
