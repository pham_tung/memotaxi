#ifndef LOGINFORM_H
#define LOGINFORM_H

#include <QDialog>
#include "transport/tcptransportadapter.h"
#include "config/deviceconfig.h"
#include "virtualkeyboard.h"
namespace Ui {
class LoginForm;
}

class LoginForm : public QDialog
{
    Q_OBJECT
public:
    explicit LoginForm(QWidget *parent = 0);
    ~LoginForm();

private slots:
    void on_btnLogin_clicked();


protected:
    bool eventFilter(QObject *, QEvent *);
private:
    Ui::LoginForm *ui;
    DeviceCfg *cfg ;
    TcpTransportAdapter* tcp_adapter;
    VirtualKeyboard* keyboard;

    bool isSending;
};

#endif // LOGINFORM_H
