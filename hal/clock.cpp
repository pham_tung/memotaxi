#include "clock.h"
#include <QDebug>
#include <QTimer>
#include <QDateTime>
#include <QUuid>
namespace hal{
static QString bytes_to_date(char date[5])
{
    if (date[0] == 0)
        return "0";
    QString dateStr = //QString::number(date[0] + 2000) + "-" +
            QString("%1-%2-%3 %4:%5").arg(date[0] + 2000)
            .arg(date[1], 2, 10, QChar('0'))
            .arg(date[2], 2, 10, QChar('0'))
            .arg(date[3], 2, 10, QChar('0'))
            .arg(date[4], 2, 10, QChar('0'));
    //qDebug()<< dateStr ;//<< "\t" << QDateTime::fromString(dateStr,"yyyy-MM-dd HH:mm");

    return QString::number(QDateTime::fromString(dateStr,"yyyy-MM-dd HH:mm").toTime_t());
}

static QString char3_to_int(char byte[3])
{
    int ret = (byte[0]<<16) + (byte[1]<<8) + byte[2];
    //    qDebug() << QString::number(byte[0] << 16)
    //             << QString::number(byte[1] << 8)
    //             << QString::number(byte[2])
    //                << ret;
    return QString::number(ret);
}

DongHo::DongHo(QObject *parent): QObject(parent)
{
    isEnd = false;
    baa_count = 0;
}

DongHo::~DongHo()
{
    close();
}


// opens the serial port specified py path

bool DongHo::open(QString path) {
    port = new QextSerialPort(path,QextSerialPort::Polling);
    timer = new QTimer(this);
    timer->setInterval(500);
    connect(timer, SIGNAL(timeout()), SLOT(got_data()));
    if (!port->open(QIODevice::ReadWrite))
        return false;
    //qDebug() << "aa";
    // set optionas
    port->setBaudRate(BAUD9600);
    port->setDataBits(DATA_8);
    port->setFlowControl(FLOW_OFF);
    port->setParity(PAR_NONE);

    connect(port,SIGNAL(readyRead()),this,SLOT(got_data()));
    usleep(10000);
    timer->start();
    return true;
}

// close the serial port
void DongHo::close() {
    port->close();
}


void DongHo::got_data()
{
    if (port->bytesAvailable()>=34)
    {
        //data.append( port->readAll());
        data = port->readAll();
        //qDebug() << data.toHex() << data.length();


        // process data
        if (isEnd)
        {
            int index = 0;
            int tmp_index = 0;
            //            for m in re.finditer('BAA|BA1',data):
            do
            {
                index = data.indexOf("BAA",index);
                tmp_index = data.indexOf("BA1",index);

                if (index == -1 && tmp_index == -1) baa_count = 50;
                else
                {
                    if (tmp_index != -1)
                    {
                        //data_baa.append(data.mid(index,34));
                        //index = tmp_index + 34;
                    }
                    else
                    {
                        data_baa.append(data.mid(index,32));
                        index += 32;
                    }
                    baa_count ++;
                    qDebug() << "found BAA at: " << index << baa_count
                             << "BA1 at: " << tmp_index;
                }
                if (baa_count >= 50)
                {
                    baa_count = 0;
                    isEnd = false;
                    qDebug() << data_baa.length() ;//<< data_baa.toHex();
                    break;
                }
            }while (index != -1);
        }
        else
        {
            if (data.startsWith("BA6"))
            {

                isEnd = true;
                baa_count = 0;
                data_baa.clear();

                //self.BAA = DataStream()
                //        self.BAA.appendbytes(data)
                //        print "start BAA"
                //#print binascii.hexlify(data)
                qDebug() << "BA6";
                data_baa.append(data);
            }
            else if (data[0] == 'B' && data[1] == 'A'
                     && data[2] > '0' && data[2] < '6'
                     && data[33] != m_current_record.Checksum
                     && data.length() == 34)
            {
                QDataStream d_stream(data);

                d_stream.readRawData(m_current_record.Ten,3);

                d_stream >> m_current_record.Vantoc;

                d_stream.readRawData(m_current_record.ThoiGianXuongXe,5);

                d_stream >> m_current_record.TrangThai;
                d_stream >> m_current_record.KmRong;
                d_stream.readRawData(m_current_record.ThoiGianLenXe,5);

                d_stream >> m_current_record.TrangThaiHongNgoai;
                d_stream.readRawData(m_current_record.KmCoKhach,3);
                d_stream >> m_current_record.ThoiGianCho;
                d_stream >> m_current_record.ThoiGianChoFree;
                d_stream >> m_current_record.SoCa;
                d_stream.readRawData(m_current_record.Tien,3);
                d_stream.readRawData(m_current_record.KmTichLuy,3);
                d_stream >> m_current_record.Checksum;

                char ten[2];
                ten[0] = 'C';
                ten[1] = data[2];
                QString content = QString("%1 %2 %3 %4 %5 %6 %7 %8 %9 %10 %11 %12 %13 %14")
                        .arg(ten)
                        .arg(QString(QUuid::createUuid().toByteArray().toBase64()))
                        .arg(QString::number(m_current_record.Vantoc))
                        .arg(bytes_to_date(m_current_record.ThoiGianXuongXe))
                        .arg(QString::number(m_current_record.TrangThai))
                        .arg(QString::number(m_current_record.KmRong))
                        .arg(bytes_to_date(m_current_record.ThoiGianLenXe))
                        .arg(QString::number(m_current_record.TrangThaiHongNgoai))
                        .arg(char3_to_int(m_current_record.KmCoKhach))
                        .arg(QString::number(m_current_record.ThoiGianCho))
                        .arg(QString::number(m_current_record.ThoiGianChoFree))
                        .arg(QString::number(m_current_record.SoCa))
                        .arg(char3_to_int(m_current_record.Tien))
                        .arg(char3_to_int(m_current_record.KmTichLuy));
                //qDebug() <<content;
//                qDebug() << m_current_record.Ten << m_current_record.Vantoc
//                         << bytes_to_date(m_current_record.ThoiGianXuongXe) << m_current_record.TrangThai
//                         << m_current_record.KmRong << bytes_to_date(m_current_record.ThoiGianLenXe)
//                         << m_current_record.TrangThaiHongNgoai << char3_to_int(m_current_record.KmCoKhach)
//                         << m_current_record.ThoiGianCho << m_current_record.ThoiGianChoFree
//                         << m_current_record.SoCa << char3_to_int(m_current_record.Tien)
//                         << char3_to_int(m_current_record.KmTichLuy) << m_current_record.Checksum;
                emit GotData(content);
            }
        }
    }
}

}
