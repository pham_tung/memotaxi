#include "gpiometer.h"
#ifdef RPI
#include "wiringPi.h"
#include <QTime>

namespace hal{
GpioMeter::GpioMeter(QObject *parent) :
    QThread(parent),m_count(0),m_velocty(0),m_vel_count(0)
{
    timer = new QTimer();
    timer->setInterval(100);
    timer->setSingleShot(false);
    connect(timer,SIGNAL(timeout()),this,SLOT(timer_timeout()));
}

GpioMeter::~GpioMeter()
{
    delete timer;
}

void GpioMeter::timer_timeout()
{
    m_vel_count++;
    double distance = 0;
    distance = m_count * 1000 / 2530;
    if (m_vel_count >= 10)
    {
        m_velocty = m_velocty * 3600 / 2530;
        m_vel_count = 0;
        emit event_report(distance,m_velocty);
        m_velocty = 0;
    }else
        emit event_report(distance,-1);



}
void GpioMeter::resetCounter()
{
    m_count = 0;
    m_velocty = 0;
    m_vel_count = 0;
}

void GpioMeter::run(){
    system("gpio edge 21 falling");
    wiringPiSetupSys();
    piHiPri(99);
    timer->start();
    Q_FOREVER{
        if (waitForInterrupt(21,-1) > 0)
        {
            m_count ++;
            m_velocty ++;
        }
    }
}
}
#endif
