#ifndef CLOCK_H
#define CLOCK_H
#include <qextserialport.h>
#include <QObject>
#include <QTimer>
namespace hal{


class BARecord
{
public:
    char Ten[3];
    quint8 Vantoc;
    char ThoiGianXuongXe[5];
    quint8 TrangThai;
    quint16 KmRong;
    char ThoiGianLenXe[5];
    quint8 TrangThaiHongNgoai;
    char KmCoKhach[3];
    quint16 ThoiGianCho;
    quint16 ThoiGianChoFree;
    quint16 SoCa;
    char Tien[3];
    char KmTichLuy[3];
    quint8 Checksum;
} ;


class DongHo: public QObject
{

    Q_OBJECT

public:
    explicit DongHo(QObject *parent = 0);
    ~DongHo();
    bool open(QString path);
    void close();

    void write(quint8 byte);
    void write(QString str);

    void init();
    void reset();
public slots:
    void got_data();

signals:
    void GotData(QString rec);
private:
    QextSerialPort *port;
    QByteArray data;
    QTimer* timer;

    BARecord m_current_record;
    bool isEnd;
    int baa_count;
    QByteArray data_baa;
};


}
#endif // CLOCK_H
