#ifndef GPIOMETER_H
#define GPIOMETER_H
#include "rpi.h"
#ifdef RPI
#include <QTimer>
#include <QThread>
namespace hal{
class GpioMeter : public QThread
{
    Q_OBJECT
public:
    explicit GpioMeter(QObject *parent = 0);
    ~GpioMeter();
    
    void resetCounter();
signals:
    void event_report(double distance,double velocty);
public slots:
    void timer_timeout();
protected:
    void run();
private:
    int m_velocty;
    int m_count;
    int m_vel_count;
    QTimer* timer ;
};
}
#endif // RPI
#endif // GPIOMETER_H
