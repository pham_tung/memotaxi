#ifndef CHECK
#define CHECK
#include "loginform.h"
#include "global.h"
#include "dialog.h"
class checker : public QObject
{
    Q_OBJECT
    LoginForm *loginFrm ;

public:
    checker(QObject * o = 0) : QObject(o) {

        connect(CoreInstance.getAdapter(),SIGNAL(got_task(QVariantMap)),
                this,SLOT(check(QVariantMap)));

        CoreInstance.loginToken();
        loginFrm = new LoginForm();
    }

public slots:
    void check(QVariantMap task) {

        if (task["command"].toString() == supported_command[DriverLoginByToken] )
        {
            if (task["Error"].toInt() == 0 )
            {
                disconnect(CoreInstance.getAdapter(),SIGNAL(got_task(QVariantMap)),
                           this,SLOT(check(QVariantMap)));

                Dialog*   mainFrm = new Dialog();
#ifdef RPI_FULLSCREEN
                mainFrm->showFullScreen();
#else
                mainFrm->show();
#endif
            }
            else
            {
                if (task["Error"].toInt() == 1)
                {

#ifdef RPI_FULLSCREEN
                    loginFrm->showFullScreen();
#else
                    loginFrm->show();
#endif
                }

            }
        }
        else if (task["command"].toString() == supported_command[DriverLoginNew] && task["Error"].toInt() == 0)
        {
            disconnect(CoreInstance.getAdapter(),SIGNAL(got_task(QVariantMap)),
                       this,SLOT(check(QVariantMap)));
            loginFrm->hide();
            Dialog*   mainFrm = new Dialog();
#ifdef RPI_FULLSCREEN
            mainFrm->showFullScreen();
#else
            mainFrm->show();
#endif

        }}
};
#endif // CHECK

