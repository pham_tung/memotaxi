#!/bin/sh

DIR="/var/tmp/HS/"
if [ ! -d "$DIR" ]; then
    mkdir $DIR
fi
now=$(date)

cd $DIR
n=1
while [ $n -le 5 ]
do
    if ps -e | grep "wvdial" > /dev/null
    then
        echo $now ": found wvdial instance, hmm sleep"
    else
        echo $now ": call wvdial"
        wvdial
    fi
done
