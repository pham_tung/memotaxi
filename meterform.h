#ifndef METERFORM_H
#define METERFORM_H

#include <QTimer>
#include <QWidget>
#include <QTime>
#include <QDebug>
#include "global.h"
#ifdef RPI
#include "hal/gpiometer.h"
using namespace hal;
#endif
namespace Ui {
class MeterForm;
}

class MeterForm : public QWidget
{
    Q_OBJECT

public:
    explicit MeterForm(QWidget *parent = 0);
    ~MeterForm();


public slots:
    void process_meter(double distance,double velocty);
    void timer_eslapsed();
private slots:
    void on_btnImHere_clicked();

    void on_btnCancel_clicked();
    void on_btnMoney_clicked();

signals:
    void cancel();
private:
#ifdef RPI
    GpioMeter* meter;
#endif
    Ui::MeterForm *ui;
    QTimer* m_timer;
    QTime m_wait_time;
    double m_last_distance;
    double m_current_distance;
    double m_current_gap;
    int m_display_total;
    int m_waiting_fare;
    int m_waiting_time;
    int m_waiting_money;
    double m_delta_d;
    bool is_stop;
    void calculateFare(double distance);

    bool isPickup;
};

#endif // METERFORM_H
