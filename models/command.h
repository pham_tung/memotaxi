#ifndef DEVICECOMMAND_H
#define DEVICECOMMAND_H
#include "common.h"
#include <QString>
#include <stdint.h>

namespace models{

class CommandFactory
{
public:
    static QString createLoginNewCmd(QString username,QString password,QString deviceID,QString OSType = "Android");
    static QString createLoginTokCmd(QString token,QString deviceID,int driverID);
    static QString createLogoutCmd();
    static QString createPickUpCmd();
    static QString createDropOffCmd();
    static QString createIamHereCmd();
    static QString createExecJobCmd(long transId);
    static QString createBidCmd(int bidID);
    static QString createCancelBidCmd(int bidID);
    static QString createUpdatePositionCmd(double latitude, double longitude);

    static QString createAckCmd();

    static QString createGpsCmd(double latitude, double longitude, long time, int vGps, int vMeter,int status);
    //C1 ID VanToc ThoiGianXuongXe TrangThai KmRong ThoiGianLenXe TrangThaiHongNgoai
    //KmCoKhach ThoiGianCho ThoiGianChoFree SoCa Tien KmTichLuy
    static QString createMeterLogCmd(QString type, uint8_t vel = 0, QString timeOut = "000000", uint8_t status = 0,
                              unsigned int zeroKm = 0, QString timeIn = "000000", uint8_t irStatus = 0,
                              unsigned int KmCoKhach = 0, unsigned int waitFreeTime = 0, uint8_t SoCa = 0,
                              uint money = 0, uint kmTichLuy = 0);
private:
    static QString createCommandGeneral(QString command, QString data="");

};

}
#endif // DEVICECOMMAND_H
