#ifndef COMMON_H
#define COMMON_H
#include <QDateTime>
#include <QString>
#include <QPair>
#include <QLinkedList>
#include <QVariant>
//#include "global.h"
#include "map"
using namespace std;
#define DEMO_GPS 1
namespace models {

#define PKG_START "_W_C_B_"
#define PKG_SEPERATOR "@@"
#define PKG_END "_W_C_E_"

#define REGEX_RESPONSE "^_W_C_B_(?<command>[\\s\\S]+)@@(?<content>[\\s\\S]+)_W_C_E_$"
#define REGEX_RESPONSE_4_8 QString("_W_C_B_([\\s\\S]+)@@([\\s\\S]+)_W_C_E_\r\n")

class Location
{

    double m_Longitude;

    double m_Latitude;

    QString m_name;

public:
    Location(QVariant data){
        setName(data.toMap()["Name"].toString());

        setLatitude(data.toMap()["Latitude"].toDouble());
        setLongitude(data.toMap()["Longtitude"].toDouble());
    }

    Location(double lon,double lat):m_Longitude(lon),m_Latitude(lat){}

    Location():m_name(""),m_Latitude(0),m_Longitude(0){}

    QString toString(){
        return "(" + m_name + ": " + QString::number(m_Latitude) + "," + QString::number(m_Longitude) + ")";
    }

    double Longitude() const
    {
        return m_Longitude;
    }
    double Latitude() const
    {
        return m_Latitude;
    }
    QString Name() const
    {
        return m_name;
    }

    void setName(QString arg)
    {
        m_name = arg;
    }
    void setLatitude(double arg)
    {
        m_Latitude = arg;
    }
    void setLongitude(double arg)
    {
        m_Longitude = arg;
    }
};

static bool operator==(const Location& a1, const Location& a2) {
    return a1.Latitude() == a2.Latitude() &&
            a1.Longitude() == a2.Longitude() &&
            a1.Name() == a2.Name();
}

enum SupportedCommand
{
    DriverLoginNew = 0,
    DriverLoginByToken,
    PickUp,
    DropOff,
    IamHere,
    ExecuteJob,
    CancelBid,
    Bid,
    UpdatePosition,
    DriverLogout,

    UpdateJob,
    FinishOrder,

    NotSelected,
    CancelOrder,
    Ack,
    MeterLogC1,
    MeterLogC2,
    MeterLogC3,
    MeterLogC4,
    MeterLogC5,
    MeterLogC8,
    MeterLogCA,
    MeterLogGPS,

};

static map<SupportedCommand,QString> create_commands() {
    map<SupportedCommand,QString> commands;
    commands[DriverLoginNew] = QString("CarLoginNew");
    commands[DriverLoginByToken] = QString("CarLoginByToken");
    commands[PickUp] = QString("PickUp");
    commands[DropOff] = QString("DropOff");
    commands[IamHere] = QString("IamHere");
    commands[ExecuteJob] = QString("ExecuteJob");
    commands[CancelBid] = QString("CancelBid");
    commands[Bid] = QString("Bid");
    commands[UpdatePosition] = QString("UpdatePosition");
    commands[DriverLogout] = QString("DriverLogout");
    commands[UpdateJob] = QString("UpdateJob");
    commands[FinishOrder] = QString("FinishOrder");
    commands[NotSelected] = QString("NotSelected");
    commands[CancelOrder] = QString("CancelOrder");
    commands[MeterLogC1] = QString("C1");
    commands[MeterLogC2] = QString("C2");
    commands[MeterLogC3] = QString("C3");
    commands[MeterLogC4] = QString("C4");
    commands[MeterLogC5] = QString("C5");
    commands[MeterLogC8] = QString("C8");
    commands[MeterLogCA] = QString("CA");
    commands[MeterLogGPS] = QString("GPS");

    commands[Ack] = QString("Ack");

    return commands;
};
static map<SupportedCommand,QString> supported_command = create_commands();

class Job
{
    long m_OrderId;

    Location m_From;

    Location m_To;

    double m_Distance;

    int m_OrderState;

    bool m_IsBid;

    bool m_IsSchedule;

    QString m_NoteToDriver;

public:
    long OrderId() const
    {
        return m_OrderId;
    }
    Location From() const
    {
        return m_From;
    }

    Location To() const
    {
        return m_To;
    }

    double Distance() const
    {
        return m_Distance;
    }

    int OrderState() const
    {
        return m_OrderState;
    }

    bool Bid() const
    {
        return m_IsBid;
    }

    bool Schedule() const
    {
        return m_IsSchedule;
    }

    QString NoteToDriver() const
    {
        return m_NoteToDriver;
    }

    void setOrderId(long arg)
    {
        m_OrderId = arg;
    }
    void setFrom(Location arg)
    {
        m_From = arg;
    }
    void setTo(Location arg)
    {
        m_To = arg;
    }
    void setDistance(double arg)
    {
        m_Distance = arg;
    }
    void setOrderState(int arg)
    {
        m_OrderState = arg;
    }
    void setBid(bool arg)
    {
        m_IsBid = arg;
    }
    void setSchedule(bool arg)
    {
        m_IsSchedule = arg;
    }
    void setNote(QString arg)
    {
        m_NoteToDriver = arg;
    }
};

static bool operator==(const Job& a1, const Job& a2) {
    return a1.OrderId() == a2.OrderId() &&
            a1.From() == a2.From() &&
            a1.To() == a2.To() &&
            a1.Distance() == a2.Distance() &&
            a1.Bid() == a2.Bid() &&
            a1.OrderState() == a2.OrderState() &&
            a1.NoteToDriver() == a2.NoteToDriver() ;
}

typedef struct
{
    QString name;
    QDateTime time;
    QString content;
    uint ttl;
}CoreTask;
}
#endif // COMMON_H
