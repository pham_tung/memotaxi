#include "core.h"
#include "command.h"
//#include <QRegularExpression>
//#include <QRegularExpressionMatch>
#include <QVariantMap>
#ifdef RPI
#include <qjson/parser.h>

#else
#include <qjson/parser>
#endif
using namespace models;

DeviceCore::DeviceCore(QObject *parent) : QThread(parent),
    is_reading(false),isPerformingTask(false),m_running(false)
{
    /* read settings */
    //QString fname = QDir::home().absolutePath();
    //fname = "settings.ini";
    cfg= new DeviceCfg( "settings.ini" );
    cfg->load();

    setServerUrl(cfg->getString("Network","ServerURL","cheaptaxi.com.vn"));
    setServerPort(cfg->getInt("Network","ServerPort",5566));
    setToken(cfg->getString("Device","Token",""));
    setDeviceID(cfg->getString("Device","DeviceId","2"));
    setDriverID(cfg->getInt("Device","DriverId",2));
    setOSType(cfg->getString("Device","OSType","Android"));

    printSettings();
    adapter = new TcpTransportAdapter(ServerUrl(),ServerPort());
    connect(adapter,SIGNAL(got_task(QVariantMap)),this,SLOT(process_task(QVariantMap)));
    adapter->open();
    sleep(1);

    m_running = true;
    this->start();

    connect(&m_dong_ho,SIGNAL(GotData(QString)), this, SLOT(xu_ly_du_lieu_dong_ho(QString)));
    m_dong_ho.open("/dev/ttyUSB0");
    initGpsd();
    sleep(2);
}
void DeviceCore::initGpsd()
{
#ifdef RPI
    connect(&gpsd, SIGNAL(positionUpdated()), this, SLOT(update_position()));
    gpsd.connectToServer();
#endif
    gpsTimer.setInterval(5000);
    gpsTimer.setSingleShot(false);

    connect (&gpsTimer, SIGNAL(timeout()),this,SLOT(sendPosition()));


}

DeviceCore::~DeviceCore()
{
    m_running = false;
    //sync.unlock();
    cond.wakeAll();

    msleep(500);
    this->terminate();
    delete adapter;
    delete cfg;
}

bool DeviceCore::loginNew(QString username, QString password)
{
    QString data = CommandFactory::createLoginNewCmd(
                username, password, DeviceID(), OSType());

    CoreTask loginTask;
    loginTask.name = supported_command[DriverLoginNew];
    loginTask.time = QDateTime::currentDateTime();
    loginTask.content = data;
    //m_task_result = false;
    //res_sync.lock();
    insertTask(loginTask);
    //res_cond.wait(&res_sync);
    //res_sync.unlock();
    return true;//m_task_result;//doTask(loginTask);
}

bool DeviceCore::loginToken()
{
    CoreTask loginTask;
    loginTask.name = supported_command[DriverLoginByToken];
    loginTask.time = QDateTime::currentDateTime();
    loginTask.ttl = 0;
    loginTask.content = CommandFactory::createLoginTokCmd(Token(),DeviceID(),DriverID());
    server_res.clear();
    insertTask(loginTask);

    return true;
}

bool DeviceCore::bid(Job *job)
{
    CoreTask bidTask;
    bidTask.name = supported_command[Bid];
    bidTask.time = QDateTime::currentDateTime();
    bidTask.ttl = 0;
    bidTask.content = CommandFactory::createBidCmd(job->OrderId());
    server_res.clear();
    insertTask(bidTask);
}

bool DeviceCore::imHere()
{
    CoreTask imHereTask;
    imHereTask.name = supported_command[IamHere];
    imHereTask.time = QDateTime::currentDateTime();
    imHereTask.ttl = 0;
    imHereTask.content = CommandFactory::createIamHereCmd();
    server_res.clear();
    insertTask(imHereTask);
}

bool DeviceCore::pickUp()
{
    CoreTask pickUpTask;
    pickUpTask.name = supported_command[PickUp];
    pickUpTask.time = QDateTime::currentDateTime();
    pickUpTask.ttl = 0;
    pickUpTask.content = CommandFactory::createPickUpCmd();
    server_res.clear();
    insertTask(pickUpTask);
}

bool DeviceCore::dropOff()
{
    CoreTask dropOffTask;
    dropOffTask.name = supported_command[DropOff];
    dropOffTask.time = QDateTime::currentDateTime();
    dropOffTask.ttl = 0;
    dropOffTask.content = CommandFactory::createDropOffCmd();
    server_res.clear();
    insertTask(dropOffTask);
}

bool DeviceCore::sendPosition()
{
    CoreTask positionTask;
    positionTask.name = supported_command[MeterLogGPS];
    positionTask.time = QDateTime::currentDateTime();
    positionTask.ttl = 5;
    //positionTask.content = CommandFactory::createUpdatePositionCmd(m_current_latitude,m_current_longitude);
    positionTask.content = CommandFactory::createGpsCmd(m_current_latitude,m_current_longitude,
                                                        QDateTime::currentDateTimeUtc().toTime_t(),0,0,0);
    server_res.clear();
    insertTask(positionTask);
}

bool DeviceCore::sendAck()
{
    CoreTask ackTask;
    ackTask.name = supported_command[Ack];
    ackTask.time = QDateTime::currentDateTime();
    ackTask.ttl = 5;
    ackTask.content = CommandFactory::createAckCmd();
    server_res.clear();
    insertTask(ackTask);
}

void DeviceCore::xu_ly_du_lieu_dong_ho(QString content)
{
    CoreTask aTask;
    aTask.name = supported_command[MeterLogC1];
    aTask.time = QDateTime::currentDateTime();
    aTask.ttl = 5;
    aTask.content = content;

    //qDebug() << aTask.content;
    server_res.clear();
    insertTask(aTask);
}

void DeviceCore::process_task(QVariantMap task)
{
    //qDebug() << "got task from server: " << task["command"];

    int error_code = task["Error"].toInt();
    if (task["command"] == supported_command[DriverLoginNew])
    {
        if (error_code == 0)
        {
            QString token = task["Token"].toString();
            if (token.length() != 0)
                setToken(token);
            qDebug() << "Token updated" << Token();
        }
    }

}

bool DeviceCore::doTask(CoreTask task)
{
    //res_sync.lock();
    currentTask.name = task.name;
    currentTask.time = task.time;
    currentTask.ttl = task.ttl;
    //qDebug() << task.time << " performing task " << task.name;
    //qDebug() << task.time << " length " << task.content.length();

    return adapter->write(task.content.toStdString().c_str());
}

bool DeviceCore::insertTask(CoreTask task)
{
    sync.lock();
    //qDebug() << "Insert task " << task.name;
    m_cmd_stack.push(task);
    sync.unlock();
    cond.wakeAll();
}

void DeviceCore::run()
{

    //adapter->open();
    adapter->moveToThread(this->thread());
    while(m_running)
    {
        sync.lock();
        cond.wait(&sync);
        if (!m_running)
            break;
        if (m_cmd_stack.isEmpty())
        {
            qDebug() << "Task stack empty ...";
            sync.unlock();
            continue;
        }
        else
        {
            CoreTask currentTask = m_cmd_stack.pop();
            sync.unlock();
            // process request
            doTask(currentTask);
        }
    }
    qDebug() << "out loop";
}

void DeviceCore::setDeviceID(QString arg)
{
    m_DeviceID = arg;
}

void DeviceCore::setDriverID(int arg)
{
    m_DriverID = arg;
}

void DeviceCore::setOSType(QString arg)
{
    m_OSType = arg;
}

void DeviceCore::setServerUrl(QString arg)
{
    m_ServerUrl = arg;
}

void DeviceCore::setServerPort(int arg)
{
    m_ServerPort = arg;
}

void DeviceCore::update_position()
{
    //qDebug() << "aaa";
    //qDebug() << gpsd.gpsData()->fix.latitude << gpsd.gpsData()->fix.longitude;
#ifdef RPI
    if (gpsd.isConnected() && gpsd.flag(LATLON_SET))
    {
        if (m_current_latitude != gpsd.gpsData()->fix.latitude )
            m_current_latitude = gpsd.gpsData()->fix.latitude ;
        if (m_current_longitude != gpsd.gpsData()->fix.longitude )
            m_current_longitude = gpsd.gpsData()->fix.longitude ;
    }
#endif
}


TcpTransportAdapter *DeviceCore::getAdapter() const
{
    return adapter;
}

QString DeviceCore::Token() const
{
    return m_token;
}

QList<Job> DeviceCore::ServerJobs() const
{
    return m_jobs;
}

QString DeviceCore::OSType() const
{
    return m_OSType;
}

QString DeviceCore::DeviceID() const
{
    return m_DeviceID;
}

int DeviceCore::DriverID() const
{
    return m_DriverID;
}

void DeviceCore::setToken(QString arg)
{
    m_token = arg;
    cfg->setString("Device","Token",arg);
    cfg->save();
}

void DeviceCore::setJobs(QList<Job> arg)
{
    if (m_jobs == arg)
        return;

    m_jobs = arg;
    emit JobsUpdated(arg);
}

QString DeviceCore::ServerUrl() const
{
    return m_ServerUrl;
}

int DeviceCore::ServerPort() const
{
    return m_ServerPort;
}

void DeviceCore::printSettings()
{
    qDebug() << "* Current settings:";
    qDebug() << "* Device ID: " << DeviceID();
    qDebug() << "* OSType   : " << OSType();
    qDebug() << "* Driver ID: " << DriverID();
    qDebug() << "* Token    : " << Token();
    qDebug() << "* ServerURL: " << ServerUrl();
    qDebug() << "* ServerPort " << ServerPort();
}
