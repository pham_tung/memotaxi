#ifndef RESPONSE_H
#define RESPONSE_H


class ResponseBase :public QObject
{
    Q_OBJECT
    Q_ENUMS(SupportedCommand)

    Q_PROPERTY(SupportedCommand command READ command WRITE command)
    Q_PROPERTY(int Error READ Error)
public:

    SupportedCommand command(){return m_command;}
    int Error() const
    {
        return m_Error;
    }

public slots:
    void command(SupportedCommand arg)
    {
        m_command = arg;
    }

private:
    SupportedCommand m_command;


    int m_Error;
};

class ResponseLoginNew :public ResponseBase
{
    Q_OBJECT
    Q_PROPERTY(QString DriverName READ password)
    Q_PROPERTY(int DriverID READ deviceId)
    Q_PROPERTY(QString DriverState READ DriverState)
    Q_PROPERTY(QString Phone READ Phone)
    Q_PROPERTY(QString Avatar READ Avatar)
    Q_PROPERTY(int Rate READ Rate)
    Q_PROPERTY(QString Account READ Account)
    Q_PROPERTY(QString PersonalID READ PersonalID)
    Q_PROPERTY(QString Email READ Email)
    Q_PROPERTY(QString PIDProvince READ PIDProvince)
    Q_PROPERTY(QString LicenseNumber READ LicenseNumber)
    Q_PROPERTY(QString VehiclePlate READ VehiclePlate)
    Q_PROPERTY(QString VehicleModel READ VehicleModel)
    Q_PROPERTY(int Seat READ Seat)
    Q_PROPERTY(bool Airport READ Airport)
    Q_PROPERTY(QString Password READ Password WRITE setPassword)
    Q_PROPERTY(QString Token READ Token WRITE setToken)

    QString m_DriverName;

    QString m_VehiclePlate;

    int m_DriverID;

    QString m_DriverState;

    QString m_Phone;

    QString m_Avatar;

    int m_Rate;

    QString m_Account;

    QString m_PersonalID;

    QString m_Email;

    QString m_PIDProvince;

    QString m_LicenseNumber;

    QString m_VehicleModel;

    int m_Seat;

    bool m_Airport;

    QString m_Password;

    QString m_Token;

public :
    ResponseLoginNew(){command(DriverLoginNew);}

    QString password() const
    {
        return m_DriverName;
    }
    QString Phone() const
    {
        return m_VehiclePlate;
    }
    int deviceId() const
    {
        return m_DriverID;
    }
    QString DriverState() const
    {
        return m_DriverState;
    }
    QString Avatar() const
    {
        return m_Avatar;
    }
    int Rate() const
    {
        return m_Rate;
    }
    QString Account() const
    {
        return m_Account;
    }
    QString PersonalID() const
    {
        return m_PersonalID;
    }
    QString Email() const
    {
        return m_Email;
    }
    QString PIDProvince() const
    {
        return m_PIDProvince;
    }
    QString LicenseNumber() const
    {
        return m_LicenseNumber;
    }
    QString VehiclePlate() const
    {
        return m_VehiclePlate;
    }
    QString VehicleModel() const
    {
        return m_VehicleModel;
    }
    int Seat() const
    {
        return m_Seat;
    }
    bool Airport() const
    {
        return m_Airport;
    }
    QString Password() const
    {
        return m_Password;
    }
    QString Token() const
    {
        return m_Token;
    }

public slots:
    void setPassword(QString arg)
    {
        m_Password = arg;
    }
    void setToken(QString arg)
    {
        m_Token = arg;
    }
};

class ResponseUpdateJob :public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<Job> Jobs READ Jobs WRITE setOrderId)
    QList<Job> m_Jobs;

public:

    QList<Job> Jobs() const
    {
        return m_Jobs;
    }
public slots:
    void setOrderId(QList<Job> arg)
    {
        m_Jobs = arg;
    }
};

class Job : public QObject
{
    Q_OBJECT
    Q_PROPERTY(long OrderId READ OrderId WRITE setOrderId)
    Q_PROPERTY(Location From READ From WRITE setFrom)
    Q_PROPERTY(Location To READ To WRITE setTo)
    Q_PROPERTY(double Distance READ Distance WRITE setDistance)
    Q_PROPERTY(int OrderState READ OrderId WRITE setOrderId)
    Q_PROPERTY(bool IsBid READ OrderId WRITE setBid)
    Q_PROPERTY(QString NoteToDriver READ NoteToDriver WRITE setNote)

    long m_OrderId;

    Location m_From;

    Location m_To;

    double m_Distance;

    int m_OrderState;

    bool m_IsBid;

    QString m_NoteToDriver;

public:
    long OrderId() const
    {
        return m_OrderId;
    }
    Location From() const
    {
        return m_From;
    }

    Location To() const
    {
        return m_To;
    }

    double Distance() const
    {
        return m_Distance;
    }

    QString NoteToDriver() const
    {
        return m_NoteToDriver;
    }

public slots:
    void setOrderId(long arg)
    {
        m_OrderId = arg;
    }
    void setFrom(Location arg)
    {
        m_From = arg;
    }
    void setTo(Location arg)
    {
        m_To = arg;
    }
    void setDistance(double arg)
    {
        m_Distance = arg;
    }
    void setBid(bool arg)
    {
        m_IsBid = arg;
    }
    void setNote(QString arg)
    {
        m_NoteToDriver = arg;
    }


};
class Location
{

    double m_Longitude;

    double m_Latitude;

    QString m_name;

public:


    double Longitude() const
    {
        return m_Longitude;
    }
    double Latitude() const
    {
        return m_Latitude;
    }
    QString Name() const
    {
        return m_name;
    }

    void setName(QString arg)
    {
        m_name = arg;
    }
    void setLatitude(double arg)
    {
        m_Latitude = arg;
    }
    void setLongitude(double arg)
    {
        m_Longitude = arg;
    }
};
#endif // RESPONSE_H
