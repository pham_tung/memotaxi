#ifndef RESPONSE_H
#define RESPONSE_H
#include <QString>
#include "common.h"

namespace models{

class ResponseBase
{
public:

    SupportedCommand command(){return m_command;}
    int Error() const
    {
        return m_Error;
    }

    void command(SupportedCommand arg)
    {
        m_command = arg;
    }

private:
    SupportedCommand m_command;


    int m_Error;
};

class ResponseLoginNew
{
    QString m_DriverName;

    QString m_VehiclePlate;

    int m_DriverID;

    QString m_DriverState;

    QString m_Phone;

    QString m_Avatar;

    int m_Rate;

    QString m_Account;

    QString m_PersonalID;

    QString m_Email;

    QString m_PIDProvince;

    QString m_LicenseNumber;

    QString m_VehicleModel;

    int m_Seat;

    bool m_Airport;

    QString m_Password;

    QString m_Token;

public :
    ResponseLoginNew(){command(DriverLoginNew);}

    QString password() const
    {
        return m_DriverName;
    }
    QString Phone() const
    {
        return m_VehiclePlate;
    }
    int deviceId() const
    {
        return m_DriverID;
    }
    QString DriverState() const
    {
        return m_DriverState;
    }
    QString Avatar() const
    {
        return m_Avatar;
    }
    int Rate() const
    {
        return m_Rate;
    }
    QString Account() const
    {
        return m_Account;
    }
    QString PersonalID() const
    {
        return m_PersonalID;
    }
    QString Email() const
    {
        return m_Email;
    }
    QString PIDProvince() const
    {
        return m_PIDProvince;
    }
    QString LicenseNumber() const
    {
        return m_LicenseNumber;
    }
    QString VehiclePlate() const
    {
        return m_VehiclePlate;
    }
    QString VehicleModel() const
    {
        return m_VehicleModel;
    }
    int Seat() const
    {
        return m_Seat;
    }
    bool Airport() const
    {
        return m_Airport;
    }
    QString Password() const
    {
        return m_Password;
    }
    QString Token() const
    {
        return m_Token;
    }

    void setPassword(QString arg)
    {
        m_Password = arg;
    }
    void setToken(QString arg)
    {
        m_Token = arg;
    }
};


class ResponseUpdateJob
{

    QList<Job> m_Jobs;

public:

    QList<Job> Jobs() const
    {
        return m_Jobs;
    }
};


}
#endif // RESPONSE_H
