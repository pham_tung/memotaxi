#include "command.h"
#include <QVariant>
#include <QDebug>
#include "rpi.h"
#ifdef RPI
#include <qjson/serializer.h>
#else
#include <qjson/serializer>
#endif
using namespace models;

QString CommandFactory::createLoginNewCmd(QString username, QString password,
                                          QString deviceID, QString OSType)
{
    QVariantMap deviceinfo;

    deviceinfo.insert("DeviceID",deviceID);
    deviceinfo.insert("OSType",OSType);
    deviceinfo.insert("PubTok","");
    deviceinfo.insert("Password",password);
    deviceinfo.insert("UserName",username);

    QJson::Serializer serializer;
    bool ok;
    QByteArray data = serializer.serialize(deviceinfo,&ok);
    if (ok)
        return createCommandGeneral(supported_command[DriverLoginNew],data);
    return "";
}

QString CommandFactory::createLoginTokCmd(QString token, QString deviceID, int driverID)
{
    QVariantMap deviceinfo;

    deviceinfo.insert("Token",token);
    deviceinfo.insert("DeviceID",deviceID);
    deviceinfo.insert("DriverID",driverID);

    QJson::Serializer serializer;
    bool ok;
    QByteArray data = serializer.serialize(deviceinfo,&ok);
    if (ok)
        return createCommandGeneral(supported_command[DriverLoginByToken],data);
    return "";
}

QString CommandFactory::createPickUpCmd()
{
    return createCommandGeneral(supported_command[PickUp]);
}

QString CommandFactory::createDropOffCmd()
{
    return createCommandGeneral(supported_command[DropOff]);
}

QString CommandFactory::createIamHereCmd()
{
    return createCommandGeneral(supported_command[IamHere]);
}

QString CommandFactory::createBidCmd(int bidID)
{
    QVariantMap deviceinfo;

    deviceinfo.insert("OrderId",bidID);

    QJson::Serializer serializer;
    bool ok;
    QByteArray data = serializer.serialize(deviceinfo,&ok);
    if (ok)
        return createCommandGeneral(supported_command[Bid],data);
    return "";
}


QString CommandFactory::createCommandGeneral(QString command, QString data)
{
    return PKG_START + command + PKG_SEPERATOR +
            data + PKG_END + "\r\n";
}


QString CommandFactory::createUpdatePositionCmd(double latitude, double longitude)
{
    QVariantMap deviceinfo;

    deviceinfo.insert("Latitude",latitude);
    deviceinfo.insert("Longitude",longitude);

    QJson::Serializer serializer;
    bool ok;
    QByteArray data = serializer.serialize(deviceinfo,&ok);
    if (ok)
        return createCommandGeneral(supported_command[UpdatePosition],data);
    return "";
}

QString CommandFactory::createAckCmd()
{
    return createCommandGeneral(supported_command[Ack]);
}

QString CommandFactory::createGpsCmd(double latitude, double longitude, long time, int vGps, int vMeter,int status)
{
    QString data;
    data = supported_command[MeterLogGPS] + " " +
            QString::number(latitude) + " " + QString::number(longitude) + " " +
            QString::number(vGps) + " " + QString::number(vMeter) + " " +
            QString::number(status) + " " + QString::number(time) ;
    //qDebug() << data;
    return data;
}

QString CommandFactory::createMeterLogCmd(QString type, uint8_t vel, QString timeOut,
                                          uint8_t status ,
                                          unsigned int zeroKm , QString timeIn ,
                                          uint8_t irStatus ,
                                          unsigned int KmCoKhach ,
                                          unsigned int waitFreeTime, uint8_t SoCa,
                                          uint money, uint kmTichLuy)
{
    QByteArray data;

    qDebug() << type << vel << timeOut << status << zeroKm
             << timeIn << irStatus << KmCoKhach <<
                waitFreeTime << SoCa << money << kmTichLuy;
    //qDebug() << data;
    //return createCommandGeneral(type,data);
    return "";

}
