#ifndef DEVICETRANSPORT_H
#define DEVICETRANSPORT_H

#include <QStack>
#include <QMutex>
#include <QWaitCondition>
#include <QThread>
#include "config/deviceconfig.h"
#include "command.h"

#include "transport/tcptransportadapter.h"
#include "rpi.h"
#include <QTimer>
#ifdef RPI
#include <hal/gpsd.h>
#include <hal/clock.h>
using namespace hal;
#endif
namespace models {
class DeviceCore : public QThread
{
    Q_OBJECT
    // Device property
    Q_PROPERTY(QString Token READ Token WRITE setToken)
    Q_PROPERTY(QString OSType READ OSType WRITE setOSType)
    Q_PROPERTY(QString DeviceID READ DeviceID WRITE setDeviceID)
    Q_PROPERTY(int DriverID READ DriverID WRITE setDriverID)
    // Network property
    Q_PROPERTY(QString ServerUrl READ ServerUrl WRITE setServerUrl)
    Q_PROPERTY(int ServerPort READ ServerPort WRITE setServerPort)
    // App property
    Q_PROPERTY(QList<Job> ServerJobs READ ServerJobs WRITE setJobs NOTIFY JobsUpdated)
public:
    explicit DeviceCore(QObject *parent = 0);
    ~DeviceCore();
    bool loginNew(QString username,QString password);
    bool loginToken();

    bool bid(Job* job);
    bool imHere();
    bool pickUp();
    bool dropOff();
    void printSettings();
    //bool handleLoginNew(QString json);
    //bool handleLoginToken(QString json);

    QString Token() const;

    QString DeviceID() const;

    int DriverID() const;

    QString OSType() const;

    QList<Job> ServerJobs() const;

    QString ServerUrl() const;

    int ServerPort() const;

    TcpTransportAdapter *getAdapter() const;
#ifdef RPI
    void startGps(){gpsTimer.start();}
    void stopGps(){gpsTimer.stop();}
#endif

public slots:
    void setToken(QString arg);
    void process_task(QVariantMap task);

    void setJobs(QList<Job> arg);

    bool insertTask(CoreTask task);
    bool doTask(CoreTask task);


    void setDeviceID(QString arg);

    void setDriverID(int arg);

    void setOSType(QString arg);

    void setServerUrl(QString arg);

    void setServerPort(int arg);

    void update_position();
    bool sendPosition();
    bool sendAck();

    void xu_ly_du_lieu_dong_ho(QString content);
signals:
    void JobsUpdated(QList<Job> arg);

private:
    bool isPerformingTask;
    TcpTransportAdapter* adapter;

    DeviceCfg *cfg;

    bool is_reading;
    QList<Job> m_jobs;

    QStack<CoreTask> m_cmd_stack;
    QMutex sync;
    QWaitCondition cond;

    QVariantMap server_res;
//    QByteArray m_Response;
//    QMutex adapter_sync;
//    QWaitCondition adapter_cond;
//    bool is_sync;

//    bool m_task_result;
//    bool m_task_notify;
    bool m_running;

    // Core property
    QString m_token;
    QString m_DeviceID;
    int m_DriverID;
    QString m_OSType;

    QString m_ServerUrl;

    int m_ServerPort;
    CoreTask currentTask;

#ifdef RPI
    QTimer gpsTimer;
    Gpsd gpsd;

    DongHo m_dong_ho;
#endif
    double m_current_latitude;
    double m_current_longitude;

    void initGpsd();
protected:
    void run();

};
}
#endif // DEVICETRANSPORT_H
