#ifndef DEMO_GPS
#define DEMO_GPS
#include <QLinkedList>
#include "common.h"

namespace models {
class GpsDemoSource{

public:
    GpsDemoSource(){
        demo_gps = create_gps();
        iterator(&demo_gps);
    }

    static QLinkedList<Location>  create_gps() {
        QLinkedList<Location> gps;

        gps.append(Location(21.036571, 105.831100));
        gps.append(Location(21.036571, 105.831100));
        gps.append(Location(21.036571, 105.831100));
        gps.append(Location(21.035971, 105.831218));
        gps.append(Location(21.035420, 105.831379));
        gps.append(Location(21.034979, 105.831540));
        gps.append(Location(21.034468, 105.831733));
        gps.append(Location(21.034428, 105.830854));
        gps.append(Location(21.034499, 105.829749));
        gps.append(Location(21.034579, 105.828912));
        gps.append(Location(21.034689, 105.828043));
        gps.append(Location(21.034699, 105.827335));
        gps.append(Location(21.034799, 105.826305));
        gps.append(Location(21.034849, 105.825596));
        gps.append(Location(21.034949, 105.824685));
        gps.append(Location(21.035009, 105.824073));
        gps.append(Location(21.035149, 105.822517));
        gps.append(Location(21.035230, 105.821273));
        gps.append(Location(21.035340, 105.820339));
        gps.append(Location(21.035380, 105.819266));
        gps.append(Location(21.035575, 105.817909));
        gps.append(Location(21.036381, 105.816412));
        gps.append(Location(21.036832, 105.815597));
        gps.append(Location(21.038204, 105.815425));
        gps.append(Location(21.040136, 105.816112));
        gps.append(Location(21.041378, 105.816423));
        gps.append(Location(21.042129, 105.817936));
        gps.append(Location(21.042129, 105.819856));
        gps.append(Location(21.035575, 105.817909));
        gps.append(Location(21.041436, 105.822645));
        gps.append(Location(21.039634, 105.827763));
        gps.append(Location(21.038853, 105.829029));
        gps.append(Location(21.037811, 105.829887));
        gps.append(Location(21.036429, 105.831132));

        return gps;
    };

    QLinkedList<Location> demo_gps;
    QLinkedListIterator iterator;
    Location getLocation()
    {
        // move to front if iterator reach the end
        if (!iterator.hasNext())
            iterator.toFront();

        return iterator.next();

    }
};
}
#endif // DEMO_GPS

