#include "driverinfodialog.h"
#include "ui_driverinfodialog.h"

DriverInfoDialog::DriverInfoDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DriverInfoDialog)
{
    ui->setupUi(this);
}

DriverInfoDialog::~DriverInfoDialog()
{
    delete ui;
}
