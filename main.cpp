#include <QApplication>
#include "global.h"

#include <QWidget>
#include <QObject>
#include <QFileInfo>
#include "config/deviceconfig.h"
#include <QTimer>
#include "checker.h"
#include "hal/gpiometer.h"
#include "hal/clock.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFileInfo fCfg ("settings.ini");
    if (!fCfg.exists()){
        DeviceCfg cfg("settings.ini");
        cfg.setString("Network","ServerURL","");
        cfg.setInt("Network","ServerPort",5566);

        cfg.setString("Device","DeviceId","2");
        cfg.setString("Device","OSType","Android");
        cfg.setString("Device","Token","");
        cfg.setString("Device","DriverId","");

        cfg.save();
    }

    //Dialog*   mainFrm = new Dialog();
    //mainFrm->showFullScreen();
    //DongHo clock;
    //clock.open("/dev/ttyUSB0");

    checker appstarter;

    return a.exec();
}
